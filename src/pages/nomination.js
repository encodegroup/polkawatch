import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { ChartSection } from "../views"
import { DataBarChart } from "../components/databarchart"
import { ChartLegend } from "../components/chartlegend"
import {  BiMoney } from 'react-icons/bi';

const SecondPage = () => (
  <Layout>
    <Seo title="Nomination" />
    <ChartSection
      title={"Nomination"}
      description={"Nomninator count by nominator value range"}
      chart={<DataBarChart/>}
      legend={<ChartLegend data={[{key:"Nominators"}]} icon={BiMoney}/>}
    />
  </Layout>
)

export default SecondPage
