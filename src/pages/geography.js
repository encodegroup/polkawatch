import * as React from "react"

import {ChartSection} from "../views";

import {DataPieChart} from "../components/datapiechart"
import {ChartLegend} from "../components/chartlegend";
import {DataStackedChart} from "../components/datastakedchart";

import {BiMap} from 'react-icons/bi';


import dataGeoRewardDistribution from "../data/pack/geo-reward-distribution.yaml"
import dataGeoRewardEvolution from "../data/pack/geo-reward-evolution.yaml";

import Layout from "../components/layout"
import Seo from "../components/seo"

const SecondPage = () => {

  return (
    <Layout>
      <Seo title="Geography"/>
      <ChartSection
        title={"Geographic Distribution"}
        description={"Staking rewards by Region of the validator"}
        chart={<DataPieChart data={dataGeoRewardDistribution}/>}
        legend={<ChartLegend data={dataGeoRewardDistribution} icon={BiMap}/>}
      />

      <ChartSection
        title={"Geographic Distribution Evolution"}
        description={"Evolution of staking rewards by region of the validator"}
        chart={<DataStackedChart data={dataGeoRewardEvolution}/>}
        legend={<ChartLegend data={dataGeoRewardDistribution} icon={BiMap}/>}
      />

    </Layout>
  )
}

export default SecondPage
