import React from "react";

import { ReactElement } from 'react';
import { chakra, Center, Box, SimpleGrid, Icon, Text, Stack, Flex, Link } from '@chakra-ui/react';

interface FeatureProps {
  title: string;
  text: string;
  icon: ReactElement;
  href: string;
}

const Feature = ({ title, text, icon, href }: FeatureProps) => {
  return (
    <Stack>
      <Center>
        <Flex
          w={16}
          h={16}
          align={'center'}
          justify={'center'}
          color={'white'}
          rounded={'full'}
          bg={'green.400'}
          mb={1}>
          {icon}
        </Flex>
      </Center>
      <Link href={href}>
        <Text fontWeight={600} align={"center"}>{title}</Text>
      </Link>
      <Text color={'gray.600'} align={"center"}>{text}</Text>
    </Stack>
  );
};

export function FeaturesSection({title, description, features, columns=2 }) {
  return (
    <Box p={4}>
      <chakra.h2
        textAlign={'center'}
        fontSize={'4xl'}
        py={10}
        fontWeight={'bold'}>
        {title}
      </chakra.h2>
      <Text textAlign={'center'} color={'gray.600'}>
        {description}
      </Text>
      <SimpleGrid columns={{ base: 1, md: columns }} spacing={10}>
      {features.map((f,i)=>(
          <Feature
            icon={<Icon as={f.icon} w={10} h={10} />}
            title={f.title}
            text={f.description}
            href={f.href}
          />
        ))}
      </SimpleGrid>
    </Box>
  );
}
