const { src, dest, series } = require("gulp");

const fs = require('fs').promises;
const yaml = require('js-yaml');

const {Client} = require("@elastic/elasticsearch");
const elastic = new Client({ node: 'http://localhost:9200' });
const INDEX="pw_reward";
const TARGET_DIR="./src/data/pack"

async function fetchDataPack(query,name,transform= x=>x){
  console.log(`procesing data pack query ${name}`);

  let result = await elastic.search({
    index: INDEX,
    track_total_hits: true,
    body: query
  });

  // Apply the supplied transformation
  result=transform(result);

  // get rid of types that YAML is picky about.
  result=JSON.parse(JSON.stringify(result));

  await fs.writeFile(`${TARGET_DIR}/${name}.yaml`, yaml.dump(result))
}


/**
 * Read the data pack queries
 */
const {

  // About Data Sources
  aboutDataEras,
  aboutDataRewards,
  aboutDataDotRewards,

  // Geo
  geoRewardDistribution,
  geoRewardEvolution,
  geoValidatorCountries,

  // net

  netRewardDistribution,
  netRewardEvolution,
  netValidatorNetwork,

  // validator

  validatorGroupDistribution,

  // nominator

  nominatorValueHistogram

} = require("./datasrc");


async function dataPackTask() {

  // about data

  await fetchDataPack(aboutDataEras,"about-data-eras", r => r.body.aggregations[0]);
  await fetchDataPack(aboutDataRewards,"about-data-rewards", r => r.body.hits.total);
  await fetchDataPack(aboutDataDotRewards,"about-data-dot-rewards", r => r.body.aggregations[0]);

  // geo data

  await fetchDataPack(geoRewardDistribution,"geo-reward-distribution", r => r.body.aggregations[0].buckets.map(i => ({
    key: i.key,
    value: i["1"].value
  })));

  await fetchDataPack(geoRewardEvolution,"geo-reward-evolution", r => r.body.aggregations[0]);
  await fetchDataPack(geoValidatorCountries,"geo-validator-countries", r => r.body.aggregations[0].buckets.map(i => ({
    key: i.key,
    value: i["1"].value
  })));

  // network data

  await fetchDataPack(netRewardDistribution,"net-reward-distribution", r => r.body.aggregations[0].buckets.map(i => ({
    key: i.key,
    value: i["1"].value
  })));

  await fetchDataPack(netRewardEvolution,"net-reward-evolution", r => r.body.aggregations[0]);
  await fetchDataPack(netValidatorNetwork,"net-validator-networks", r => r.body.aggregations[0].buckets.map(i => ({
    key: i.key,
    value: i["1"].value
  })));


  // validator data

  await fetchDataPack(validatorGroupDistribution,"validator-group-distribution", r => r.body.aggregations[0].buckets.map(i => ({
    key: i.key,
    dot_reward: i["2"].value,
    median_nomination: i["1"].values["50.0"]
  })));

  // nominator data

  await fetchDataPack(nominatorValueHistogram,"nominator-value-histogram", r => {
    // in histogram buckets are keys in an object
    let buckets=r.body.aggregations[0].buckets;
    let keys=Object.keys(buckets);

    return keys.map(k => ({
      name: buckets[k].from.toString() + ' - ' + buckets[k].to,
      // dot_to:  buckets[k].to,
      nominator_stake_value:  buckets[k]["1"].value,
    }))
  });

}

exports.default = dataPackTask;