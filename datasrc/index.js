
// Exports the queries to process

exports.aboutDataEras = require("./aboutDataEras");
exports.aboutDataRewards = require("./aboutDataRewards");
exports.aboutDataDotRewards = require("./aboutDataDotReward");


// geo information

exports.geoRewardDistribution = require("./geoRewardDistribution");
exports.geoRewardEvolution = require("./geoRewardEvolution");
exports.geoValidatorCountries = require("./geoValidatorCountries");


// network information

exports.netRewardDistribution = require("./netRewardDistribution");
exports.netRewardEvolution = require("./netRewardEvolution");
exports.netValidatorNetwork = require("./netValidatorNetwork");

// validator information

exports.validatorGroupDistribution = require("./validatorGroupDistribution");

// nominator information

exports.nominatorValueHistogram = require("./nominatorValueHistogram");