import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import {ValidatorTable} from "../views"

import dataValidatorGroupDistribution from "../data/pack/validator-group-distribution.yaml"

const SecondPage = () => (
  <Layout>
    <Seo title="Validators" />
    <ValidatorTable
      data={dataValidatorGroupDistribution}
      caption={'Dot rewards and median nomination size per validator'}
      column_names={['name', 'dot reward', 'median nomination']}
    />
  </Layout>
)

export default SecondPage
