module.exports={
  "aggs": {
    "0": {
      "range": {
        "script": {
          "source": "doc['nomination_value'].value/10000000000.0 ",
          "lang": "painless"
        },
        "ranges": [
          {
            "to": 120,
            "from": 0
          },
          {
            "to": 250,
            "from": 120
          },
          {
            "to": 500,
            "from": 250
          },
          {
            "to": 1000,
            "from": 500
          },
          {
            "to": 2000,
            "from": 1000
          },
          {
            "to": 3000,
            "from": 2000
          },
          {
            "to": 5000,
            "from": 3000
          },
          {
            "to": 10000,
            "from": 5000
          },
          {
            "to": 30000,
            "from": 10000
          },
          {
            "to": 100000,
            "from": 30000
          },
          {
            "to": 500000,
            "from": 100000
          },
          {
            "to": 1000000,
            "from": 500000
          },
          {
            "to": 2000000,
            "from": 1000000
          },
          {
            "from": 2000000
          }
        ],
        "keyed": true
      },
      "aggs": {
        "1": {
          "cardinality": {
            "field": "nominator"
          }
        }
      }
    }
  },
  "size": 0,
  "fields": [
    {
      "field": "date",
      "format": "date_time"
    }
  ],
  "script_fields": {
    "reward_dot": {
      "script": {
        "source": "doc['reward'].value/10000000000.0\n",
        "lang": "painless"
      }
    },
    "nomination_value_dot": {
      "script": {
        "source": "doc['nomination_value'].value/10000000000.0 ",
        "lang": "painless"
      }
    }
  },
  "stored_fields": [
    "*"
  ],
  "runtime_mappings": {},
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "range": {
            "era": {
              "gte": 505
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
