import React, { PureComponent } from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend } from 'recharts';

import {ColorForItem} from "./palette";

const transposeData = (dta) => {

  const dataset = [

  ];

  const dataKeys = [];
  Object.keys(dta.buckets).forEach(function (key) {
    const dataObject = {}
    Object.keys(dta.buckets[key]['1'].buckets).forEach(function (country_key) {
      dataObject[dta.buckets[key]['1'].buckets[country_key].key.toString().replace(/\s/g, "_")] = Math.round(dta.buckets[key]['1'].buckets[country_key]['2'].value);
      if (!dataKeys.includes(dta.buckets[key]['1'].buckets[country_key].key.toString().replace(/\s/g, "_"))) {
        dataKeys.push(dta.buckets[key]['1'].buckets[country_key].key.toString().replace(/\s/g, "_"));
      }
    });
    dataObject['name'] = dta.buckets[key].key;
    dataset.push(dataObject);
  });

  const transposed = {
    dataset: dataset,
    data_keys: dataKeys
  }

  return transposed;
}


export class DataStackedChart extends PureComponent {

  render() {
    const data = transposeData(this.props.data);
    const dataKeys = data.data_keys

    return (
      <ResponsiveContainer width="100%" height="100%">
        <AreaChart
          width={400}
          height={400}
          data={data.dataset}
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          {dataKeys.map((dataKey, index) => (
            <Area dataKey={dataKey} stackId="1" stroke="#8884d8" fill={ColorForItem(index)} />
          ))}
        </AreaChart>
      </ResponsiveContainer>
    );
  }
}
