

export const ProjectData={
  name: "Polkawatch",
  copyright:"© 2021 PolkaWatch Project Contributors",
  url: "https://polkawatch.app",
  gitlab_url:"https://gitlab.com/polkawatch/",
  discord_url: "https://discord.gg/464nqZX3xb"
}