

export default {
  "aggs": {
    "0": {
      "terms": {
        "field": "validator_country_group_name",
        "order": {
          "1": "desc"
        },
        "size": 10
      },
      "aggs": {
        "1": {
          "sum": {
            "script": {
              "source": "doc['reward'].value/10000000000.0\n",
              "lang": "painless"
            }
          }
        }
      }
    }
  },
  "size": 0,
  "fields": [
    {
      "field": "date",
      "format": "date_time"
    }
  ],
  "script_fields": {
    "reward_dot": {
      "script": {
        "source": "doc['reward'].value/10000000000.0\n",
        "lang": "painless"
      }
    },
    "nomination_value_dot": {
      "script": {
        "source": "doc['nomination_value'].value/10000000000.0 ",
        "lang": "painless"
      }
    }
  },
  "stored_fields": [
    "*"
  ],
  "runtime_mappings": {},
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "range": {
            "date": {
              "format": "strict_date_optional_time",
              "gte": "2021-10-13T09:18:56.458Z",
              "lte": "2021-10-28T09:18:56.458Z"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
