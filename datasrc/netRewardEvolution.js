module.exports={
  "aggs": {
    "0": {
      "histogram": {
        "field": "era",
        "interval": 1,
        "min_doc_count": 1
      },
      "aggs": {
        "1": {
          "terms": {
            "field": "validator_asn_group",
            "order": {
              "2": "desc"
            },
            "size": 6
          },
          "aggs": {
            "2": {
              "sum": {
                "script": {
                  "source": "doc['reward'].value/10000000000.0\n",
                  "lang": "painless"
                }
              }
            }
          }
        }
      }
    }
  },
  "size": 0,
  "fields": [
    {
      "field": "date",
      "format": "date_time"
    }
  ],
  "script_fields": {
    "reward_dot": {
      "script": {
        "source": "doc['reward'].value/10000000000.0\n",
        "lang": "painless"
      }
    },
    "nomination_value_dot": {
      "script": {
        "source": "doc['nomination_value'].value/10000000000.0 ",
        "lang": "painless"
      }
    }
  },
  "stored_fields": [
    "*"
  ],
  "runtime_mappings": {},
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "range": {
            "era": {
              "gte": 505,
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}