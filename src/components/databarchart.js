import React, { PureComponent } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import dataNominatorValueHistogram from "../data/pack/nominator-value-histogram.yaml"

import {ColorForItem} from "./palette";




export class DataBarChart extends PureComponent {

  render() {
    console.log(dataNominatorValueHistogram);
    return (
      <ResponsiveContainer width="100%" height="100%">
        <BarChart width={150} height={40} data={dataNominatorValueHistogram}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="nominator_stake_value" fill={ColorForItem(0)} />
        </BarChart>
      </ResponsiveContainer>
    );
  }
}

