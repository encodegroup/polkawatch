import React from "react";

import {
  Flex,
  Text,
  Stack,
  StackDivider,
  Icon,
  useColorModeValue,
} from '@chakra-ui/react';

import { ReactElement } from 'react';

import {ColorForItem} from "./palette";

interface FeatureProps {
  text: string;
  iconBg: string;
  icon?: ReactElement;
}

const Feature = ({ text, icon, iconBg }: FeatureProps) => {
  return (
    <Stack direction={'row'} align={'center'}>
      <Flex
        w={8}
        h={8}
        align={'center'}
        justify={'center'}
        rounded={'full'}
        bg={iconBg}>
        {icon}
      </Flex>
      <Text fontWeight={600}>{text}</Text>
    </Stack>
  );
};

export function ChartLegend({data,icon}) {
  return (
    <Stack
      spacing={4}
      divider={
        <StackDivider
          borderColor={useColorModeValue('gray.100', 'gray.700')}
        />
      }>

      {data.map((d,i)=>(
        <Feature
          icon={
            <Icon as={icon} color={'white'} w={5} h={5} />
          }
          iconBg={useColorModeValue(ColorForItem(i), ColorForItem(i))}
          text={d.key}
        />
      ))}
    </Stack>
  )}


